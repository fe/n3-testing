# n3-testing

This is supposed to be a test-bubble for the rewrite of the ROGER-frontend as a React-component.

What is tested?

1. The use of a recent version of the N3 lib in general.
1. The dev-stack for a React component using this lib with
    - a preview-app using the ESM build and
    - hot-reloading.

## Installation

```sh
npm run i-all
```

## Development

```sh
npm run dev
```

## Production Build

Common JS and ESM bundles are built into the `dist/` directory.

```sh
npm run build
```

## Preview App: Changes made over the vanilla `create-react-app`

The preview app is bootstrapped with create-react-app and the component in development is listed as a dependency.

```json
"dependencies": {
    "n3-testing": "file:.."
  }
```

Also, the `App`-component of the bootstrapped app has been replaced with the `N3testing`-component and all unreferenced resource files have been purged.

```js
import { N3testing } from 'n3-testing';

ReactDOM.render(
  <React.StrictMode>
    <N3testing />
  </React.StrictMode>,
  document.getElementById("root")
);
```
