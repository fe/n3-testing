import React from "react";
import {
  Table,
  TableBody,
  TableCell,
  TableHead,
  TableRow,
} from "@mui/material";

const N3 = require("n3");

const parser = new N3.Parser();
parser.parse(
  `PREFIX c: <http://example.org/cartoons#>
   c:Tom a c:Cat.
   c:Jerry a c:Mouse;
           c:smarterThan c:Tom.`,
  (error, quad, prefixes) => {
    if (quad) console.log(quad);
    else console.log("# That's all, folks!", prefixes);
  }
);

const { DataFactory } = N3;
const { namedNode, literal, defaultGraph, quad } = DataFactory;
const myQuad = quad(
  namedNode("https://ruben.verborgh.org/profile/#me"),
  namedNode("http://xmlns.com/foaf/0.1/givenName"),
  literal("Ruben", "en"),
  defaultGraph()
);

export default function N3testing() {
  return (
    <Table>
      <TableHead>
        <TableRow>
          <TableCell>Variable</TableCell>
          <TableCell>supposed outcome</TableCell>
          <TableCell>actual outcome</TableCell>
        </TableRow>
      </TableHead>
      <TableBody>
        <TableRow>
          <TableCell>termType</TableCell>
          <TableCell>Quad</TableCell>
          <TableCell>{myQuad.termType}</TableCell>
        </TableRow>
        <TableRow>
          <TableCell>value</TableCell>
          <TableCell></TableCell>
          <TableCell>{myQuad.value}</TableCell>
        </TableRow>
        <TableRow>
          <TableCell>subject.value</TableCell>
          <TableCell>https://ruben.verborgh.org/profile/#me</TableCell>
          <TableCell>{myQuad.subject.value}</TableCell>
        </TableRow>
        <TableRow>
          <TableCell>object.value</TableCell>
          <TableCell>Ruben</TableCell>
          <TableCell>{myQuad.object.value}</TableCell>
        </TableRow>
        <TableRow>
          <TableCell>object.datatype.value</TableCell>
          <TableCell>
            http://www.w3.org/1999/02/22-rdf-syntax-ns#langString
          </TableCell>
          <TableCell>{myQuad.object.datatype.value}</TableCell>
        </TableRow>
        <TableRow>
          <TableCell>object.language</TableCell>
          <TableCell>en</TableCell>
          <TableCell>{myQuad.object.language}</TableCell>
        </TableRow>
      </TableBody>
    </Table>
  );
}
