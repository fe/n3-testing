import React from "react";
import ReactDOM from "react-dom";
import { N3testing } from 'n3-testing';

ReactDOM.render(
  <React.StrictMode>
    <N3testing />
  </React.StrictMode>,
  document.getElementById("root")
);
